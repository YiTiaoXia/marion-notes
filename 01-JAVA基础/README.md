## JAVA基础

## 第1章 视频教程
### 1.1 [尚硅谷Java入门视频教程(在线答疑+Java面试真题)](https://www.bilibili.com/video/BV1Kb411W75N)
 ### 1.2 网盘资料
 - 链接: [https://pan.baidu.com/s/1tew0XmmnGcGSzvinoaNXzQ](https://pan.baidu.com/s/1tew0XmmnGcGSzvinoaNXzQ)
 - 提取码: wel0
 ### 1.3 同学笔记
 - [https://blog.csdn.net/PorkBird/article/details/113666542](https://blog.csdn.net/PorkBird/article/details/113666542)

## 第2章 电子书籍
1. [《JAVA编程的逻辑》](/)

## 第3章 博客文章

## 第4章 面试知识点